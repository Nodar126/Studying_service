# Studying_service
LMS-система, в которой каждый желающий может размещать свои полезные материалы или курсы.

Команды:
- python manage.py get_payment_for_test - создается 1 пользователь, 1 менеджер, 1 курс\урок и 3 платежа;
- celery -A config worker -l INFO -P eventlet - асинхронная рассылка писем пользователям об обновлении материалов курса; # Windows
- celery -A config beat -l info -S django - фоновая задача, которая проверяет пользователей по дате последнего входа; # Windows
- docker-compose build - собрать Docker-образ;
- docker-compose up - запуск контейнера.
