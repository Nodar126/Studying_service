from django.core.management import BaseCommand

from study.models import Payment, Course, Lesson
from users.models import User


class Command(BaseCommand):
    moderator = User.objects.create(email='thop@gmail.com', role='moderator')
    moderator.set_password('fwet43t34t3464')
    moderator.save()

    user = User.objects.create(email='young@gmail.com', role='member')
    user.set_password('trh4y5y54y54')
    user.save()

    course = Course.objects.create(title='Курс 1', overview='Курс 1')
    lesson = Lesson.objects.create(title='Урок 1', overview='Урок  1')
    # user = User.objects.all().first()
    # course = Course.objects.all().first()
    # lesson = Lesson.objects.all().first()

    def handle(self, *args, **kwargs):
        Payment.objects.create(
            date='2022-05-27',
            user=self.user,
            price=100,
            method='0',
            course=self.course,
        )
        Payment.objects.create(
            date='2021-08-07',
            user=self.user,
            price=150,
            method='1',
            lesson=self.lesson
        )
        Payment.objects.create(
            date='2020-05-28',
            user=self.user,
            price=75,
            method='0',
            course=self.course,
        )
