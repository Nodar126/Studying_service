# Generated by Django 4.2.5 on 2023-09-25 15:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('study', '0002_alter_course_preview_alter_lesson_preview'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lesson',
            name='link',
            field=models.CharField(blank=True, max_length=150, null=True, verbose_name='ссылка'),
        ),
    ]
