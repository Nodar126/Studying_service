from celery import shared_task
from django.core.mail import send_mail

from study.models import Subscription


@shared_task
def check_update(instance_pk, instance_title, instance_user_email, instance_user_pk):
    if Subscription.objects.filter(user=instance_user_pk, course=instance_pk):
        send_mail(
            subject=f'Курс {instance_title}',
            message='Курс, на который вы подписаны был обновлен!',
            from_email=None,
            recipient_list={instance_user_email}
        )
