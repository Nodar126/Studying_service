from datetime import datetime
from django.db import models

from config import settings
from users.models import NULLABLE


# Create your models here.
class Course(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, **NULLABLE)
    title = models.CharField(max_length=150, verbose_name='название')
    preview = models.ImageField(verbose_name='превью (картинка)', **NULLABLE)
    overview = models.TextField(verbose_name='описание')
    amount = models.IntegerField(default=100, verbose_name='цена')

    class Meta:
        verbose_name = 'Курс'
        verbose_name_plural = 'Курсы'


class Lesson(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, **NULLABLE)
    title = models.CharField(max_length=150, verbose_name='название')
    overview = models.TextField(verbose_name='описание')
    preview = models.ImageField(verbose_name='превью (картинка)', **NULLABLE)
    link = models.CharField(max_length=150, verbose_name='ссылка', **NULLABLE)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, **NULLABLE)

    class Meta:
        verbose_name = 'Урок'
        verbose_name_plural = 'Уроки'


class Payment(models.Model):
    METHOD_CHOICES = (
        ('0', 'Cash'),
        ('1', 'Card'),
    )

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, **NULLABLE)
    date = models.DateField(auto_now_add=datetime.now, verbose_name='дата')
    course = models.ForeignKey(Course, on_delete=models.CASCADE, **NULLABLE)
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE, **NULLABLE)
    price = models.IntegerField(verbose_name='цена', **NULLABLE)
    method = models.CharField(max_length=4, choices=METHOD_CHOICES, default='Card', verbose_name='способ')
    stripe_id = models.TextField(verbose_name='идентификатор платежа', **NULLABLE)

    class Meta:
        verbose_name = 'Платеж'
        verbose_name_plural = 'Платежи'


class Subscription(models.Model):

    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, **NULLABLE)
    course = models.OneToOneField(Course, on_delete=models.CASCADE)
    status = models.BooleanField(default=True, verbose_name='статус активности')

    class Meta:
        verbose_name = 'Подписка'
        verbose_name_plural = 'Подписки'
