from rest_framework import serializers

VALID_VIDEO_SERVICE = 'youtube'


def validator_scam_videos(value):
    if VALID_VIDEO_SERVICE not in set(value.lower().split('.')):
        raise serializers.ValidationError('Введен запрещенный сервис.')
