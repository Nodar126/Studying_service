import stripe
from rest_framework import serializers

from config import settings
from study.models import Course, Lesson, Payment, Subscription
from study.validators import validator_scam_videos


class LessonSerializer(serializers.ModelSerializer):
    link = serializers.CharField(validators=[validator_scam_videos], read_only=True)

    class Meta:
        model = Lesson
        fields = '__all__'


class CourseSerializer(serializers.ModelSerializer):
    lesson_count = serializers.SerializerMethodField()
    lessons_list = serializers.SerializerMethodField()
    subscription_status = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Course
        fields = '__all__'

    def get_lesson_count(self, instance):
        return instance.lesson_set.count()

    def get_lessons_list(self, instance):
        return instance.lesson_set.all()

    def get_subscription_status(self, instance):
        if hasattr(instance, 'subscription'):
            return instance.subscription.status
        return False


class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = '__all__'

    def validate(self, data):
        if not data.get('course'):
            raise serializers.ValidationError("Выберите курс.")
        return data


class PaymentStripeSerializer(PaymentSerializer):
    stripe = serializers.SerializerMethodField()

    class Meta:
        model = Payment
        fields = '__all__'

    def get_stripe(self, instance):
        stripe.api_key = settings.PAY_API_KEY
        stripe_data = stripe.PaymentIntent.retrieve(
            instance.stripe_id,
        )
        return stripe_data


class SubscriptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Subscription
        fields = ('course', 'status')
