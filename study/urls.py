from django.urls import path

from rest_framework.routers import DefaultRouter

from study.apps import StudyConfig
from study.views import CourseViewSet, LessonCreateAPIView, LessonListApiView, LessonRetrieveAPIView, \
    LessonUpdateAPIView, LessonDestroyAPIView, PaymentListApiView, SubscriptionCreateAPIView, \
    SubscriptionDestroyAPIView, PaymentCreateAPIView, PaymentRetrieveAPIView

app_name = StudyConfig.name

router = DefaultRouter()
router.register(r'course', CourseViewSet, basename='courses')

urlpatterns = [
    path('subscription/create/', SubscriptionCreateAPIView.as_view(), name='create_subscription'),
    path('subscription/delete/<int:pk>', SubscriptionDestroyAPIView.as_view(), name='subscription_delete'),
    path('lessons/create/', LessonCreateAPIView.as_view(), name='create_lesson'),
    path('lessons/', LessonListApiView.as_view(), name='lessons_list'),
    path('lessons/list/<int:pk>', LessonRetrieveAPIView.as_view(), name='lesson_get'),
    path('lessons/update/<int:pk>', LessonUpdateAPIView.as_view(), name='lesson_update'),
    path('lessons/delete/<int:pk>', LessonDestroyAPIView.as_view(), name='lesson_delete'),
    path('payment/', PaymentListApiView.as_view(), name='payment_list'),
    path('payment/create/', PaymentCreateAPIView.as_view(), name='create_payment'),
    path('payment/list/<int:pk>', PaymentRetrieveAPIView.as_view(), name='payment_get'),
] + router.urls
